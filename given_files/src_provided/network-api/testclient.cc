#include <iostream>
#include <pthread.h>
#include "server-network-api.hh"
#include "client-network-api.hh"
#include "common.hh"

int main(void)
{
  //network_api::ServerNetworkAPI server(8080);
  network_api::ClientNetworkAPI client("127.0.0.1", "4242");
  std::cout << "CONNEXION ESTABLISHED WITH PLAYER1" << std::endl;
  std::cout << client.acknowledge("gilles-aristide.nyinke-engo") << std::endl;
  
  
  network_api::ClientNetworkAPI client2("10.224.33.20", "8080");
  std::cout << "CONNEXION ESTABLISHED WITH PLAYER2" << std::endl;
  std::cout << client2.acknowledge("login-test") << std::endl;
  
  std::cout << "Logins sent!" << std::endl;
  
  //communication
    std::cout << client.receive() << std::endl;//OK
    //client.send("uciok");

  return 0;
}
