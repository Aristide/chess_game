#pragma once

#include <vector>
#include <utility>
#include "piece.hh"
#include "plugin/listener.hh"
#include "plugin/chessboard-interface.hh"

class Board : public plugin::ChessboardInterface
{
  public:
    Board();
    void addListeners(std::vector<plugin::Listener*>& listeners);
    std::vector<Piece>& getBoard();
    float getTimeout();
    Piece* getPiece(const std::pair<int, int>& position);
    const Piece* getPiece(const std::pair<int, int>& position) const;
    void removePiece(Piece *piece);
    void movePiece(Piece& piece, const std::pair<int, int>& position);
    bool checkMove(Piece& piece, const std::pair<int, int>& position);
    bool checkMove2(Piece& piece, const std::pair<int, int>& position);
    bool checkRook(Piece& piece, const std::pair<int, int>& position);
    bool checkBishop(Piece& piece, const std::pair<int, int>& position);
    void print(const std::pair<int, int>& position);
    void printAll();
    void initBoard();
    bool castling(Piece& piece, const std::pair<int, int>& position);
    void getAllMoves(Piece& piece, std::vector<std::pair<int, int>>& moves);
    bool isCheck(plugin::Color color);

    opt_piece_t operator[](const plugin::Position& position) const override;

  private:

    std::vector<plugin::Listener*> listeners_;
    std::vector<Piece> pieces_;
    float timeout_;
};
