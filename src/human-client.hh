#pragma once

#include "board.hh"
#include "network-api/client-network-api.hh"

class HumanClient
{
  private:
    Board board_;
    network_api::ClientNetworkAPI client_;
  
  public:
    HumanClient(const std::string& ip, const std::string& port, 
    const std::string& login);
}

