#include "ai.hh"


int main(int argc, char *argv[])
{
  if(argc != 3)
    return 0;
  Ai ai(argv[1], argv[2]);

  return 0;
}
