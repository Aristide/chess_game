#include <iostream>
#include <utility>
#include "piece.hh"

Piece::Piece(plugin::PieceType type, plugin::Color color,
           std::pair<int, int> position) :
          type_(type), color_(color), position_(position){}

plugin::PieceType Piece::getType() const
{
  return type_;
}

plugin::Color Piece::getColor() const
{
  return color_;
}

std::pair<int, int> Piece::getPosition() const
{
  return position_;
}

void Piece::setPosition(const std::pair<int, int>& position)
{
  position_ = position;
}

void Piece::setType(plugin::PieceType type)
{
  type_ = type;
}
