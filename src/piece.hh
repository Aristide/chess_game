#pragma once

#include <utility>
#include "plugin/piece-type.hh"
#include "plugin/color.hh"

class Piece
{
  public:
    Piece(plugin::PieceType type, plugin::Color color,
                           std::pair<int, int> position);

    plugin::PieceType getType() const;
    plugin::Color getColor() const;
    std::pair<int, int> getPosition() const;
    void setPosition(const std::pair<int, int>& position);
    void setType(plugin::PieceType type);

  private:
    plugin::PieceType type_;
    plugin::Color color_;
    std::pair<int, int> position_;
};
