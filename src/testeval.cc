#include "piece.cc"
#include <vector>
#include <iostream>
#include <stdlib.h>

int evalfunct(std::vector<Piece> board, plugin::Color color, int value = 0)
{
  //int value = 0;
  int pieceIndex = rand() % board.size();
  for (auto i = board.begin(); i != board.end(); i++)
  {
    /*THE VALUES OF MY SIDE*/
    if (board[pieceIndex % board.size()].getColor() == plugin::Color::WHITE
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::PAWN)
      value += 100;

    if (board[pieceIndex % board.size()].getColor() == plugin::Color::WHITE
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::KNIGHT)
      value += 320;

    if (board[pieceIndex % board.size()].getColor() == plugin::Color::WHITE
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::BISHOP)
      value += 330;

    if (board[pieceIndex % board.size()].getColor() == plugin::Color::WHITE
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::ROOK)
      value += 500;

    if (board[pieceIndex % board.size()].getColor() == plugin::Color::WHITE
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::QUEEN)
      value += 900;

    if (board[pieceIndex % board.size()].getColor() == plugin::Color::WHITE
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::KING)
      value += 20000;
    
    else
    {
      value = 0;
    /*OPPONENT VALUES*/
     if (board[pieceIndex % board.size()].getColor() == color
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::PAWN)
      value -= 100;

    if (board[pieceIndex % board.size()].getColor() == color
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::KNIGHT)
      value -= 320;

    if (board[pieceIndex % board.size()].getColor() == color
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::BISHOP)
      value -= 330;

    if (board[pieceIndex % board.size()].getColor() == color
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::ROOK)
      value -= 500;

    if (board[pieceIndex % board.size()].getColor() == color
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::QUEEN)
      value -= 900;

    if (board[pieceIndex % board.size()].getColor() == color
        && board[pieceIndex % board.size()].getType() == plugin::PieceType::KING)
      value -= 20000;
  }
  }

  return value;
}


int main(void)
{
  std::vector<Piece> pieces_;
  
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::WHITE, std::pair<int, int>(0, 0)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::WHITE, std::pair<int, int>(1, 0)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::WHITE, std::pair<int, int>(2, 0)));
  pieces_.push_back(Piece(plugin::PieceType::QUEEN, plugin::Color::WHITE, std::pair<int, int>(3, 0)));
  pieces_.push_back(Piece(plugin::PieceType::KING, plugin::Color::WHITE, std::pair<int, int>(4, 0)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::WHITE, std::pair<int, int>(5, 0)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::WHITE, std::pair<int, int>(6, 0)));
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::WHITE, std::pair<int, int>(7, 0)));                                                                                
  for(int i = 0; i < 8; i++)                                                       
    pieces_.push_back(Piece(plugin::PieceType::PAWN, plugin::Color::WHITE, std::pair<int, int>(i, 1)));
                                                                                   
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::BLACK, std::pair<int, int>(0, 7)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::BLACK, std::pair<int, int>(1, 7)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::BLACK, std::pair<int, int>(2, 7)));
  pieces_.push_back(Piece(plugin::PieceType::QUEEN, plugin::Color::BLACK, std::pair<int, int>(3, 7)));
  pieces_.push_back(Piece(plugin::PieceType::KING, plugin::Color::BLACK, std::pair<int, int>(4, 7)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::BLACK, std::pair<int, int>(5, 7)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::BLACK, std::pair<int, int>(6, 7)));
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::BLACK, std::pair<int, int>(7, 7)));
  for(int i = 0; i < 8; i++)                                                       
    pieces_.push_back(Piece(plugin::PieceType::PAWN, plugin::Color::BLACK, std::pair<int, int>(i, 6)));
                                                                                   
  
  std::cout << evalfunct(pieces_, plugin::Color::BLACK, 0) << std::endl;
  std::cout << evalfunct(pieces_, plugin::Color::WHITE, 0) << std::endl;
  
  return 0;
}
