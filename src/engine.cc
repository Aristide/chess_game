#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>
#include <regex>
#include "engine.hh"
#include "board.hh"

Engine::Engine(int port, std::vector<plugin::Listener*>& listeners_)
{
  board.addListeners(listeners_);
  (void) port;
  std::vector<std::pair<int, int>> moves;

  board.printAll();
  board.getAllMoves(*board.getPiece(std::pair<int, int>(4, 4)), moves);
  std::cout << board.checkMove(*board.getPiece(std::pair<int, int>(4, 4)), std::pair<int, int>(4, 7)) << '\n';
  std::cout << board.isCheck(*board.getPiece(std::pair<int, int>(4, 7))) << '\n';
}

Engine::Engine(char *path, std::vector<plugin::Listener*>& listeners_)
{
  board.addListeners(listeners_);
  listeners = listeners_;
  parse_pgn(path);
  for(auto i = listeners_.begin(); i != listeners_.end(); i++)
    (*i)->on_game_finished();
}

void Engine::parse_pgn(char *path)
{
  std::ifstream file(path);
  std::string str((std::istreambuf_iterator<char>(file)),
		  std::istreambuf_iterator<char>());
  file.close();

  str = str.substr(str.find("\n\n") + 2);
  std::regex exp("(O-O)|(O-O-O)|([A-Z]?[a-h][1-8][-|x][a-h][1-8][+|#]?)");
  std::sregex_iterator begin(str.begin(), str.end(), exp);
  std::sregex_iterator end;

  for(auto i = begin; i != end; i++){
    parse_move(i->str().c_str());
  }
}

void Engine::parse_move(const char *move)
{
  static bool colorWhite = false;

  if(strcmp(move, "O-O") == 0)
  {
    if(colorWhite)
      move = std::string("e1-g1").c_str();
    else
      move = std::string("e8-g8").c_str();
  }
  if(strcmp(move, "O-O-0") == 0)
  {
    if(colorWhite)
      move = std::string("e1-c1").c_str();
    else
      move = std::string("e8-c8").c_str();
  }
  int i = 0;
  if(move[i] >= 'A' && move[i] <= 'Z')
    i++;
  int x = (int)(move[i++] - 'a');
  int y = (int)(move[i++] - '1');
  Piece *piece = board.getPiece(std::pair<int, int>(x, y));
  i++;
  if(piece)
  {
    x = (int)(move[i++] - 'a');
    y = (int)(move[i++] - '1');
    board.movePiece(*piece, std::pair<int, int>(x, y));
    if(move[i] == '+')
      logCheck(false, (colorWhite ? plugin::Color::WHITE : plugin::Color::BLACK));
    else if(move[i] == '#')
      logCheck(true, (colorWhite ? plugin::Color::WHITE : plugin::Color::BLACK));
  }
  else
    std::cout << "Mechant";
  colorWhite = !colorWhite;
}

void Engine::logCheck(bool isMate, plugin::Color color)
{
  for(auto i = listeners.begin(); i != listeners.end(); i++)
  {
    if(isMate)
      (*i)->on_player_mat(color);
    else
      (*i)->on_player_check(color);
  }
}

Board& Engine::getBoard()
{
  return board;
}

/***********************CONNEXION**************************/
/*first part of the appendix done: INITIALIZATION*/
/*void Engine::handle_server()
{
  svn_player1_(4242);//this instanciation is blocking function waiting conexion 4242 is an example of number of port
  svn_player1_.acknowledge(false);//attribute the color white to the client1
    value1 = 0;//check if the attribuation is ok
  
  svn_player2_(4242);//same port??
  svn_player2_.acknowledge(true);//attribuate color black to other player
//Move all numerics values and the abstract this function!!
}*/
