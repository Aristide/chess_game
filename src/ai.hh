#pragma once

#include "board.hh"
#include "plugin/color.hh"

class Ai
{
  private:
    Board board;
    plugin::Color color = plugin::Color::WHITE;

  public:
    Ai(const std::string& ip, const std::string& port);
    void getEnnemyMove(const std::string& response);
    std::string findBestMove();
    std::string pairToString(const std::pair<int, int>& origin, const std::pair<int, int>& destination);
    int evalfunct(std::vector<Piece> board, plugin::Color color, int valueGame = 0);
};
