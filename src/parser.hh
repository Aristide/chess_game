#pragma once

#include "board.hh"

class Parser
{
  private:
    std::String file_;
  
  public:
    int parser(Board board);
}
