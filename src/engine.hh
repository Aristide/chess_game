#pragma once

#include <vector>
#include "network-api/server-network-api.hh"
#include "board.hh"
#include "piece.hh"

class Engine
{
  public:
    Engine(int port, std::vector<plugin::Listener*>& listeners_);
    Engine(char *path, std::vector<plugin::Listener*>& listeners_);
    void parse_pgn(char *path);
    void parse_move(const char *move);
    void logCheck(bool isMate, plugin::Color color);
    Board& getBoard();
    void handle_server();

  private:
    Board board;
    std::vector<plugin::Listener*> listeners;
    network_api::ServerNetworkAPI *svn_player1_;
    network_api::ServerNetworkAPI *svn_player2_;
};
