#include <iostream>
#include "board.hh"

Board::Board()
{
  initBoard();
}

std::optional<std::pair<plugin::PieceType, plugin::Color>> Board::operator[](const plugin::Position& position) const
{
  const Piece *piece = getPiece(std::pair<int, int>((int)position.file_get(), (int)position.rank_get()));
  if(piece)
    return std::pair<plugin::PieceType, plugin::Color>(piece->getType(), piece->getColor());
  else
    return std::nullopt;
}

void Board::addListeners(std::vector<plugin::Listener*>& listeners)
{
  listeners_ = listeners;
  for(auto i = listeners_.begin(); i != listeners_.end(); i++)
  {
    (*i)->register_board(*this);
    (*i)->on_game_started();
  }
}

std::vector<Piece>& Board::getBoard()
{
  return pieces_;
}

float Board::getTimeout()
{
  return timeout_;
}

Piece* Board::getPiece(const std::pair<int, int>& position)
{
  for(unsigned long i = 0; i < pieces_.size(); i++)
  {
    if(pieces_[i].getPosition() == position)
      return &pieces_[i];
  }
  return nullptr;
}

const Piece* Board::getPiece(const std::pair<int, int>& position) const
{
  for(unsigned long i = 0; i < pieces_.size(); i++)
  {
    if(pieces_[i].getPosition() == position)
      return &pieces_[i];
  }
  return nullptr;
}

void Board::removePiece(Piece *piece)
{
  for(auto i = pieces_.begin(); i != pieces_.end(); i++)
  {
    if(&(*i) == piece)
    {
      pieces_.erase(i);
      break;
    }
  }
}

bool Board::castling(Piece& piece, const std::pair<int, int>& position)
{
  int pos = (piece.getColor() == plugin::Color::WHITE ? 0 : 7);
  if(piece.getType() == plugin::PieceType::KING &&
     piece.getPosition() == std::pair<int, int>(4, pos) &&
     position == std::pair<int, int>(6, pos))
  {
    Piece *piece2 = getPiece(std::pair<int, int>(7, pos));
    if(piece2 && piece2->getType() == plugin::PieceType::ROOK)
    {
      piece.setPosition(position);
      piece2->setPosition(std::pair<int, int>(5, pos));
      plugin::Position from((plugin::File)piece.getPosition().first, (plugin::Rank)piece.getPosition().second);
      plugin::Position to((plugin::File)position.first, (plugin::Rank)position.second);
      for(auto i = listeners_.begin(); i != listeners_.end(); i++)
      {
        (*i)->on_piece_moved(piece.getType(), from, to);
        (*i)->on_kingside_castling((piece.getColor() == plugin::Color::WHITE ? plugin::Color::BLACK : plugin::Color::WHITE));
      }
      return true;
    }
  }
  else if(piece.getType() == plugin::PieceType::KING &&
     piece.getPosition() == std::pair<int, int>(4, pos) &&
     position == std::pair<int, int>(2, pos))
  {
    Piece *piece2 = getPiece(std::pair<int, int>(0, pos));
    if(piece2 && piece2->getType() == plugin::PieceType::ROOK)
    {
      piece.setPosition(position);
      piece2->setPosition(std::pair<int, int>(3, pos));
      plugin::Position from((plugin::File)piece.getPosition().first, (plugin::Rank)piece.getPosition().second);
      plugin::Position to((plugin::File)position.first, (plugin::Rank)position.second);
      for(auto i = listeners_.begin(); i != listeners_.end(); i++)
      {
        (*i)->on_piece_moved(piece.getType(), from, to);
        (*i)->on_queenside_castling((piece.getColor() == plugin::Color::WHITE ? plugin::Color::BLACK : plugin::Color::WHITE));
      }
      return true;
    }
  }
  return false;
}

void Board::movePiece(Piece& piece, const std::pair<int, int>& position)
{
  if(castling(piece, position))
    return;
  Piece *piece2 = getPiece(position);
  piece.setPosition(position);
  plugin::Position from((plugin::File)piece.getPosition().first, (plugin::Rank)piece.getPosition().second);
  plugin::Position to((plugin::File)position.first, (plugin::Rank)position.second);
  for(auto i = listeners_.begin(); i != listeners_.end(); i++)
  {
    (*i)->on_piece_moved(piece.getType(), from, to);
    if(piece2)
      (*i)->on_piece_taken(piece2->getType(), to);
  }
  if(piece2)
    removePiece(piece2);
}

bool Board::checkMove(Piece& piece, const std::pair<int, int>& position)
{
  if(checkMove2(piece, position))
  {
    auto origin = piece.getPosition();
    piece.setPosition(position);
    bool result = !isCheck(piece.getColor());
    piece.setPosition(origin);
    return result;
  }
  return false;
}

bool Board::checkMove2(Piece& piece, const std::pair<int, int>& position)
{
  if(position.first < 0 || position.first > 7 || position.second < 0 || position.second > 7 || piece.getPosition() == position)
    return false;
  Piece *piece2 = getPiece(position);
  if(piece2 && piece2->getColor() == piece.getColor())
    return false;
  std::pair<int, int> posPiece = piece.getPosition();
  switch(piece.getType())
  {
    case plugin::PieceType::ROOK:
      return checkRook(piece, position);
      break;
    case plugin::PieceType::BISHOP:
      return checkBishop(piece, position);
      break; 
    case plugin::PieceType::KING:
      if (abs(posPiece.first - position.first) <= 1 && abs(posPiece.second - position.second) <= 1)
        return true;
      break;
    case plugin::PieceType::QUEEN:
      return checkRook(piece, position) || checkBishop(piece, position);
      break;
    case plugin::PieceType::KNIGHT:
      if ((abs(posPiece.first - position.first) == 2 && abs(posPiece.second - position.second) == 1) ||
         (abs(posPiece.first - position.first) == 1 && abs(posPiece.second - position.second) == 2))
        return true;
      break;
    case plugin::PieceType::PAWN:
      int direction = (piece.getColor() == plugin::Color::WHITE ? 1 : -1);
      if (posPiece.first == position.first && !piece2)
      {
        if(posPiece.second + direction == position.second 
           || (posPiece.second + direction * 2 == position.second 
           && (posPiece.second == 1 || posPiece.second == 6) 
           && !getPiece(std::pair<int, int>(posPiece.first, posPiece.second + direction))))
          return true;
      }
      else if(abs(posPiece.first - position.first) == 1 
              && piece2 && piece2->getColor() != piece.getColor() 
              && posPiece.second + direction == position.second)
        return true;
      break;
  }
  return false;
}

void Board::getAllMoves(Piece& piece, std::vector<std::pair<int, int>>& moves)
{
  std::pair<int, int> paire;
  for(int i = 0; i < 8; i++)
  {
    for(int j = 0; j < 8; j++)
    {
      paire.first = i;
      paire.second = j;
      if(checkMove(piece, paire))
        moves.push_back(paire);
    }
  }

  //for(auto i = moves.begin(); i != moves.end(); i++)
    //std::cout << (*i).first << ' ' << (*i).second << '\n';
}

bool Board::checkRook(Piece& piece, const std::pair<int, int>& position)
{
  std::pair<int, int> posPiece = piece.getPosition();
  if(posPiece.first == position.first)
  {
    int var = (posPiece.second < position.second ? 1 : -1);
    for(int i = posPiece.second + var; i != position.second; i += var)
    {
      posPiece.second = i;
      if(getPiece(posPiece))
        return false;
    }
  }
  else if(posPiece.second == position.second)
  {
    int var = (posPiece.first < position.first ? 1 : -1);
    for(int i = posPiece.first + var; i != position.first; i += var)
    {
      posPiece.first = i;
      if(getPiece(posPiece))
        return false;
    }
  }
  else
    return false;
  return true;
}

bool Board::checkBishop(Piece& piece, const std::pair<int, int>& position)
{
  std::pair<int, int> posPiece = piece.getPosition();
  if(abs(posPiece.first - position.first) == abs(posPiece.second - position.second))
  {
    int varFirst = (posPiece.first < position.first ? 1 : -1);
    int varSecond = (posPiece.second < position.second ? 1 : -1);
    int j = posPiece.second + varSecond;
    for(int i = posPiece.first + varFirst; i != position.first; i += varFirst)
    {
      posPiece.first = i;
      posPiece.second = j;
      if(getPiece(posPiece))
        return false;
      j += varSecond;
    }
  }
  else
    return false;
  return true;
}

bool Board::isCheck(plugin::Color color)
{
  Piece *king;
  for(auto i = pieces_.begin(); i != pieces_.end(); i++)
  {
    if((*i).getType() == plugin::PieceType::KING && (*i).getColor() == color)
    {
      king = &(*i);
      break;
    }
  }

  for(auto i = pieces_.begin(); i != pieces_.end(); i++)
  {
    if((*i).getColor() != king->getColor() && checkMove(*i, king->getPosition()))
      return true;
  }
  return false;
}

void Board::print(const std::pair<int, int>& position)
{
  Piece *piece = getPiece(position);
  if(piece)
    std::cout << (char)piece->getType();
  else
    std::cout << 'O';
}

void Board::printAll()
{
  Piece *piece;
  for(int i = 0; i < 8; i++)
  {
    for(int j = 0; j < 8; j++)
    {
      piece = getPiece(std::pair<int, int>(j, 7 - i));
    if(piece)
      std::cout << (piece->getColor() == plugin::Color::BLACK ? "\033[1;30m" : "\033[1;37m") << (char)piece->getType() << "\033[0m";
    else
      std::cout << "\033[1;36m0\033[1;0m"; 
    }
    std::cout << std::endl;
  }
    std::cout << std::endl;
}

void Board::initBoard()
{
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::WHITE, std::pair<int, int>(0, 0)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::WHITE, std::pair<int, int>(1, 0)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::WHITE, std::pair<int, int>(2, 0)));
  pieces_.push_back(Piece(plugin::PieceType::QUEEN, plugin::Color::WHITE, std::pair<int, int>(3, 0)));
  pieces_.push_back(Piece(plugin::PieceType::KING, plugin::Color::WHITE, std::pair<int, int>(4, 0)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::WHITE, std::pair<int, int>(5, 0)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::WHITE, std::pair<int, int>(6, 0)));
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::WHITE, std::pair<int, int>(7, 0)));
  for(int i = 0; i < 8; i++)
    pieces_.push_back(Piece(plugin::PieceType::PAWN, plugin::Color::WHITE, std::pair<int, int>(i, 1)));

  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::BLACK, std::pair<int, int>(0, 7)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::BLACK, std::pair<int, int>(1, 7)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::BLACK, std::pair<int, int>(2, 7)));
  pieces_.push_back(Piece(plugin::PieceType::QUEEN, plugin::Color::BLACK, std::pair<int, int>(3, 7)));
  pieces_.push_back(Piece(plugin::PieceType::KING, plugin::Color::BLACK, std::pair<int, int>(4, 7)));
  pieces_.push_back(Piece(plugin::PieceType::BISHOP, plugin::Color::BLACK, std::pair<int, int>(5, 7)));
  pieces_.push_back(Piece(plugin::PieceType::KNIGHT, plugin::Color::BLACK, std::pair<int, int>(6, 7)));
  pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::BLACK, std::pair<int, int>(7, 7)));
  for(int i = 0; i < 8; i++)
    pieces_.push_back(Piece(plugin::PieceType::PAWN, plugin::Color::BLACK, std::pair<int, int>(i, 6)));

  //pieces_.push_back(Piece(plugin::PieceType::ROOK, plugin::Color::WHITE, std::pair<int, int>(4, 4)));
}
