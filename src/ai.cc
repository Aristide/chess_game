#include <iostream>
#include <pthread.h>
#include "network-api/client-network-api.hh"
#include "network-api/common.hh"
#include "ai.hh"
#include "plugin/color.hh"

#include <stdlib.h>

Ai::Ai(const std::string& ip, const std::string& port)
{
  (void) ip;(void) port;
  network_api::ClientNetworkAPI client(ip, port);
  color = (plugin::Color)client.acknowledge("gilles-aristide.nyinke-engo");

  //communication server
    client.receive();//uci
    client.send("uciok");
    client.receive();//isready
    client.send("readyok");
    client.receive();//ucinewgame

    while(1){
      std::string response = client.receive();
      if(response.substr(0, 8) == "position")
        getEnnemyMove(response);
      else if(response == "go")
        client.send(findBestMove());
    }
  /*while(1){
    board.printAll();
    std::cout << findBestMove() << '\n';
    color = (color == plugin::Color::WHITE ? plugin::Color::BLACK : plugin::Color::WHITE);
  }*/
}

std::string Ai::findBestMove()
{
  std::vector<Piece>& vector = board.getBoard();
  std::vector<std::pair<int, int>> moves;
  int pieceIndex = rand() % vector.size();
  for(auto i = vector.begin(); i != vector.end(); i++)
  {
    if(vector[pieceIndex % vector.size()].getColor() == color)
    {
      board.getAllMoves(vector[pieceIndex % vector.size()], moves);
      if(!moves.empty())
      {
	int index = rand() % moves.size();
	auto origin = vector[pieceIndex % vector.size()].getPosition();
	board.movePiece(vector[pieceIndex % vector.size()], moves[index]);
        return pairToString(origin, moves[index]);
      }
    }
    pieceIndex++;
  }
  exit(0);
  return std::string("erreur");//Cette ligne ne sera jamais exécutée.
}

void Ai::getEnnemyMove(const std::string& response)
{
  std::size_t index = response.find_last_of(' ');
  const char *move = response.substr(index + 1).c_str();
  int x = (int)(move[0] - 'a');
  int y = (int)(move[1] - '1');
  std::pair<int, int> origin(x, y);
  x = (int)(move[2] - 'a');
  y = (int)(move[3] - '1');
  std::pair<int, int> destination(x, y);

  Piece *piece = board.getPiece(origin);
  board.movePiece(*piece, destination);

  if(move[4] == 'K' || move[4] == 'Q' || move[4] == 'R' ||
     move[4] == 'B' || move[4] == 'N' || move[4] == 'P')
    piece->setType((plugin::PieceType)move[4]);
}

std::string Ai::pairToString(const std::pair<int, int>& origin, const std::pair<int, int>& destination)
{
  char move[5];
  move[0] = origin.first + 'a';
  move[1] = origin.second + '1';
  move[2] = destination.first + 'a';
  move[3] = destination.second + '1';
  move[4] = '\0';
  return std::string(move);
}

int Ai::evalfunct(Board board, plugin::Color colori, int value = 0)
{
  int pieceIndex = rand() % board.getBoard().size();
  for (auto i = board.begin(); i != board.end(); i++)
  {
    /*THE VALUES OF MY SIDE*/
    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() == color
        && board.getBoard().[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::PAWN)
      value += 100;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() == color
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::KNIGHT)
      value += 320;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() == color
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::BISHOP)
      value += 330;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() == color
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::ROOK)
      value += 500;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() == color
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::QUEEN)
      value += 900;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() == color
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::KING)
      value += 20000;

    else
    {
      value = 0;
    
    /*OPPONENT VALUES*/
     if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() ==
colori
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::PAWN)
      value -= 100;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() ==
colori
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::KNIGHT)
      value -= 320;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() ==
colori
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::BISHOP)
      value -= 330;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() ==
colori
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::ROOK)
      value -= 500;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() ==
colori
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::QUEEN)
      value -= 900;

    if (board.getBoard()[pieceIndex % board.getBoard().size()].getColor() ==
colori
        && board.getBoard()[pieceIndex % board.getBoard().size()].getType() == plugin::PieceType::KING)
      value -= 20000;
  }
  }

  return value;
}

}
