#include <iostream>
#include <dlfcn.h>
#include <string.h>
#include "engine.hh"
#include "plugin/listener.hh"

void parse_options(int argc, char *argv[]);
void addListeners(std::vector<plugin::Listener*>& listeners, char **paths);

std::vector<void*> handles;

int main(int argc, char *argv[])
{
  parse_options(argc, argv);
  for(auto i = handles.begin(); i != handles.end(); i++)
    dlclose(*i);
}

void parse_options(int argc, char *argv[])
{
  int port = -1;
  char *path = nullptr;
  std::vector<plugin::Listener*> listens;
  char **listeners = argv + argc;
  for(int i = 1; i < argc; i++)
  {
    if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
      std::cout << "Enter a port or a pgn file" << std::endl;
    if((!strcmp(argv[i], "-p") || !strcmp(argv[i], "--port")) && argv[i + 1])
      port = atoi(argv[i + 1]);
    if(!strcmp(argv[i], "--pgn") && argv[i + 1])
      path = argv[i + 1];
    if((!strcmp(argv[i], "-l") || !strcmp(argv[i], "--listener")) && argv[i + 1])
      listeners = argv + i + 1;
  }
  
  addListeners(listens, listeners);

  if(port != -1)
    Engine(port, listens);
  else if(path)
    Engine(path, listens);
  else
    std::cout << "Enter a port or a pgn file" << std::endl;
}

void addListeners(std::vector<plugin::Listener*>& listeners, char **paths)
{
  for(int i = 0; paths[i] != nullptr && paths[i][0] != '-'; i++)
  {
    void *handle;
  
    handle = dlopen(paths[i], RTLD_LAZY);
    if(!handle){
      std::cerr << "dlopen: " << dlerror() << '\n';
      continue;
    }
    auto ptr = reinterpret_cast<plugin::Listener*(*)()>(dlsym(handle, "listener_create"));
    listeners.push_back(ptr());
    handles.push_back(handle);
  }
}
