#pragma once

#include "listener.hh"

/*This class inherit from Listener class
and must implement the design pattern Observer*/

class ChessboardListener : public plugin::Listener
{
  private:
    logs_; //I dont understand the type of logs on UML("File"??)
  
  public:
    void writelogs();

}
