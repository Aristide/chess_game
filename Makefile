SOURCES=src/main.cc src/engine.cc src/board.cc src/piece.cc
OPTION=-std=c++17 -ldl -lpthread -lboost_system -Wall -Wextra -pedantic -Werror -g
EXE=chessengine

OPTION-AI=-std=c++17 -ldl -Wall -Wextra -pedantic -Werror
SOURCES-AI=src/main-ai.cc src/ai.cc src/board.cc src/piece.cc

all:
	g++ $(OPTION) $(SOURCES) -o $(EXE)

chessengine:
	g++ $(OPTION) $(SOURCES) -o $(EXE)

ai:
	g++ $(OPTION) $(SOURCES-AI) -o ai
